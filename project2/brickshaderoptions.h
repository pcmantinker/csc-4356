#ifndef BRICKSHADEROPTIONS_H
#define BRICKSHADEROPTIONS_H

#include <QDialog>
#include <QVector3D>
#include "mainwindow.h"

namespace Ui {
class BrickShaderOptions;
}

class BrickShaderOptions : public QDialog
{
    Q_OBJECT
    
public:
    explicit BrickShaderOptions(QWidget *parent = 0);
    ~BrickShaderOptions();

    QVector3D * getGLColor(QColor color);
    QColor * getQColor(QVector3D * colorVector);
    
private slots:
    void on_btnChooseMortarColor_clicked();

    void on_btnChooseBrickColor_clicked();

    void on_buttonBox_accepted();

    void on_sliderBrickSizeX_valueChanged(int value);

    void on_sliderBrickSizeY_valueChanged(int value);

    void on_sliderBrickFractionX_valueChanged(int value);

    void on_sliderBrickFractionY_valueChanged(int value);

private:
    Ui::BrickShaderOptions *ui;
    MainWindow *mainwindow;


    float sizeX;
    float sizeY;
    float fractionX;
    float fractionY;

    QVector3D *mortarColor;
    QVector3D *brickColor;
    QVector2D *brickSize;
    QVector2D *brickFraction;
};

#endif // BRICKSHADEROPTIONS_H
