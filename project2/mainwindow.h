#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector3D>
#include "glwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setBrickColor(QVector3D *color);
    void setMortarColor(QVector3D *color);
    void setBrickSize(QVector2D *size);
    void setBrickFraction(QVector2D *fraction);

    QVector3D * brickColor();
    QVector3D * mortarColor();
    QVector2D * brickSize();
    QVector2D * brickFraction();

    GLWidget *glWidget();
    
private slots:
    void on_actionExit_triggered();

    void on_actionAbout_Qt_triggered();

    void on_actionAbout_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_Framebuffer_triggered();

    void on_actionToolbar_triggered();

    void on_actionEdit_Shaders_triggered();

    void on_actionReset_Model_triggered();

    void on_actionWave_Vertex_Shader_triggered();

    void on_actionBrick_Shader_triggered();

    void updateTime();

    void on_actionBrickShaderOptions_triggered();

    void on_actionRunBrickShader_triggered();

    void on_actionExisting_triggered();

    void on_actionNew_triggered();

    void on_actionAbout_Open_GL_triggered();

private:
    Ui::MainWindow *ui;
    QString openFilesPath;
    QString saveFileName;
    GLWidget *m_glWidget;

};

#endif // MAINWINDOW_H
