#ifndef SHADERMANAGEMENT_H
#define SHADERMANAGEMENT_H

#include <QDialog>

namespace Ui {
class ShaderManagement;
}

class ShaderManagement : public QDialog
{
    Q_OBJECT
    
public:
    explicit ShaderManagement(QWidget *parent = 0);
    ~ShaderManagement();

private slots:
    void on_btnNew_clicked();

    void on_btnClose_clicked();

    void on_btnOpen_clicked();

    void on_btnDelete_clicked();

    void on_btnApply_clicked();

    void on_btnEdit_clicked();

    void vertexShaderCompileError(char *message);

    void fragmentShaderCompileError(char * message);

    void programLinkError(char *message);

private:
    Ui::ShaderManagement *ui;
    QStringList list;
};

#endif // SHADERMANAGEMENT_H
