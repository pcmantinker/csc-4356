#include "gl.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "about.h"
#include "shadereditor.h"
#include "brickshaderoptions.h"
#include "aboutopengl.h"
#include "obj.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_glWidget = new GLWidget();
    this->setCentralWidget(m_glWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTime()
{

}

void MainWindow::on_actionExit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_actionAbout_triggered()
{
    About * about = new About();
    about->exec();
}

void MainWindow::on_actionOpen_triggered()
{
    QString selectedFilter;
    QStringList files = QFileDialog::getOpenFileNames(
                this, tr("Open a Wavefront model..."),
                openFilesPath,
                tr("Wavefront (*.obj)"),
                &selectedFilter);
    if (files.count()) {
        openFilesPath = files[0];
        qDebug() << "Loading " + openFilesPath + "...";
        if(openFilesPath.contains(".obj"))
        {
            QFileInfo fi(openFilesPath);
            obj * o = obj_create(openFilesPath.toStdString().c_str());
            setWindowTitle("Project 2 - " + fi.baseName() + ".obj");
            qDebug() << "Object loaded at address " << o;
            m_glWidget->setObj(o);
        }
    }
}

void MainWindow::on_actionSave_Framebuffer_triggered()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save an image..."),
                                                    QString("image"),
                                                    tr("PNG Image (*.png);;JPEG Image (*.jpg, *.jpeg);;GIF Image (*.gif);;All Files (*)"),
                                                    &selectedFilter);
    m_glWidget->grabFrameBuffer(false).save(fileName, "PNG");
}

void MainWindow::on_actionToolbar_triggered()
{

}

void MainWindow::on_actionEdit_Shaders_triggered()
{

}


void MainWindow::on_actionReset_Model_triggered()
{
    glUseProgram(0);
    m_glWidget->updateGL();
}

void MainWindow::on_actionWave_Vertex_Shader_triggered()
{
    m_glWidget->waveShader();
}

void MainWindow::on_actionBrick_Shader_triggered()
{

}

void MainWindow::on_actionBrickShaderOptions_triggered()
{
    if(m_glWidget->currentShader().compare("brick") == 0)
    {
        BrickShaderOptions * options = new BrickShaderOptions(this);
        options->show();
    }
    else
    {
        QMessageBox::information(this, "Error", "Brick shader options cannot be set unless the brick shader is running.");
    }
}

void MainWindow::setMortarColor(QVector3D *color)
{
    m_glWidget->setMortarColor(color);
}

void MainWindow::setBrickColor(QVector3D *color)
{
    m_glWidget->setBrickColor(color);
}

void MainWindow::on_actionRunBrickShader_triggered()
{
    m_glWidget->brickShader();
}

void MainWindow::setBrickSize(QVector2D *size)
{
    m_glWidget->setBrickSize(size);
}

void MainWindow::setBrickFraction(QVector2D *fraction)
{
    m_glWidget->setBrickFraction(fraction);
}

void MainWindow::on_actionExisting_triggered()
{
    QString selectedFilter;
    QStringList files = QFileDialog::getOpenFileNames(
                this, tr("Open a shader..."),
                "shader",
                tr("Fragment or Vertex Shader (*.frag *.vert)"),
                &selectedFilter);

    if (files.count() == 2)
    {
        QString file1 = files[0];
        QString file2 = files[1];

        QString file1Type;
        QString file2Type;

        QString file1BaseName; // no path filename without extension
        QString file2BaseName; // no path filename without extension

        QString file1Name; // no path filename with extension
        QString file2Name; // no path filename with extension

        QString vertText;
        QString fragText;

        QFileInfo fi1(file1);
        file1BaseName = fi1.baseName();
        if(file1.contains(".frag"))
        {
            file1Type = "Fragment Shader";
            file1Name = fi1.baseName() + ".frag";
            QFile frag(file1);
            frag.open(QFile::ReadOnly);
            fragText = frag.readAll();
            frag.close();
        }

        if(file1.contains(".vert"))
        {
            file1Type = "Vertex Shader";
            file1Name = fi1.baseName() + ".vert";
            QFile vert(file1);
            vert.open(QFile::ReadOnly);
            vertText = vert.readAll();
            vert.close();
        }

        QFileInfo fi2(file2);
        file2BaseName = fi2.baseName();
        if(file2.contains(".frag"))
        {
            if(file1Type.compare("Fragment Shader") == 0)
            {
                QMessageBox::warning(this, "Error", "Must pick a vertex shader to pair with the fragment shader.");
                return;
            }
            file2Type = "Fragment Shader";
            file2Name = fi2.baseName() + ".frag";
            QFile frag(file2);
            frag.open(QFile::ReadOnly);
            fragText = frag.readAll();
            frag.close();
        }

        if(file2.contains(".vert"))
        {
            if(file1Type.compare("Vertex Shader") == 0)
            {
                QMessageBox::warning(this, "Error", "Must pick a fragment shader to pair with the vertex shader.");
                return;
            }
            file2Type = "Vertex Shader";
            file2Name = fi2.baseName() + ".vert";
            QFile vert(file2);
            vert.open(QFile::ReadOnly);
            vertText = vert.readAll();
            vert.close();
        }
        m_glWidget->customShader(vertText, fragText, "custom");
    }
    else
    {
        QMessageBox::warning(this, "Error", "Must choose a vertex/fragment shader pair.");
    }
}

void MainWindow::on_actionNew_triggered()
{
    ShaderEditor *se = new ShaderEditor(this);
    se->show();
}

GLWidget * MainWindow::glWidget()
{
    return m_glWidget;
}

QVector3D * MainWindow::brickColor()
{
    return m_glWidget->brickColor();
}

QVector3D * MainWindow::mortarColor()
{
    return m_glWidget->mortarColor();
}

QVector2D * MainWindow::brickSize()
{
    return m_glWidget->brickSize();
}

QVector2D * MainWindow::brickFraction()
{
    return m_glWidget->brickFraction();
}

void MainWindow::on_actionAbout_Open_GL_triggered()
{
    AboutOpenGL *aboutGL = new AboutOpenGL();
    aboutGL->exec();
}
