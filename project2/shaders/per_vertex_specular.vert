void main()
{
vec3 V = vec3(0.0, 0.0, 1.0); // View vector
vec3 L = normalize(gl_LightSource[0].position.xyz); // Light vector
vec3 N = normalize(gl_NormalMatrix * gl_Normal); // Normal vector
vec3 H = normalize(L + V); // Half-angle vector
vec4 D = gl_FrontMaterial.diffuse; // Diffuse color
vec4 S = gl_FrontMaterial.specular; // Specular color
float n = gl_FrontMaterial.shininess; // Specular exponent
float kd = max(dot(N, L), 0.0); // Diffuse intensity
float ks = pow(max(dot(N, H), 0.0), n); // Specular intensity
vec3 rgb = D.rgb * kd + S.rgb * ks; // RGB channels
float a = D.a; // Alpha channel
gl_FrontColor = vec4(rgb, a); // Vertex color
gl_Position = ftransform(); // Vertex position
}