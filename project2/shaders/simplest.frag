#version 120
varying vec3 var_L;
varying vec3 var_N;

void main()
{
    //gl_FragColor = vec4(0.4, 0.2, 1.0, 1.0);
    vec3 V = vec3(0.0, 0.0, 1.0); // View vector
    vec3 L = normalize(var_L); // Light vector
    vec3 N = normalize(var_N); // Normal vector
    vec3 H = normalize(L + V); // Half-angle vector
    vec4 D = gl_FrontMaterial.diffuse; // Diffuse color
    vec4 S = gl_FrontMaterial.specular; // Specular color
    float n = gl_FrontMaterial.shininess; // Specular exponent
    float kd = max(dot(N, L), 0.0); // Diffuse intensity
    float ks = pow(max(dot(N, H), 0.0), n); // Specular intensity
    vec3 rgb = D.rgb * kd + S.rgb * ks; // RGB channels
    float a = D.a; // Alpha channel
    gl_FragColor = vec4(rgb * vec3(0.5, 1.0, 0.3), a); // Fragment color with a green tint
}
