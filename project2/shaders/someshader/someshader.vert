varying vec3 var_L;
varying vec3 var_N;

void main(void)
{
    var_L = gl_LightSource[0].position.xyz; // Light vector
    var_N = gl_NormalMatrix * gl_Normal; // Normal vector

    gl_FrontColor = gl_FrontMaterial.diffuse; // Vertex color
    gl_Position = ftransform(); // Vertex position
}
