#version 120

uniform float time;

varying vec3 var_L;
varying vec3 var_N;

void main()
{
    vec4 P = gl_Vertex;
    P.y += sin(P.x + time * 3.0) + sin(P.z + time * 2.0);
    gl_FrontColor = gl_FrontMaterial.diffuse; // Vertex color
    gl_Position = gl_ModelViewProjectionMatrix * P; // Vertex position

    var_L = gl_LightSource[0].position.xyz; // Light vector
    var_N = gl_NormalMatrix * gl_Normal; // Normal vector
}
