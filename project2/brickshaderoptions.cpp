#include "brickshaderoptions.h"
#include "ui_brickshaderoptions.h"
#include <QtGui>

BrickShaderOptions::BrickShaderOptions(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BrickShaderOptions)
{
    ui->setupUi(this);
    mainwindow = (MainWindow*) parent;

    mortarColor = mainwindow->mortarColor();// new QVector3D(1.0f, 1.0f, 1.0f);
    brickColor = mainwindow->brickColor(); // new QVector3D(1.0f, 0.0f, 0.0f);
    brickSize = mainwindow->brickSize(); //new QVector2D(sizeX, sizeY);
    brickFraction = mainwindow->brickFraction(); //new QVector2D(fractionX, fractionY);
    sizeX = brickSize->x();
    sizeY = brickSize->y();
    fractionX = brickFraction->x();
    fractionY = brickFraction->y();

    QColor * bColor = getQColor(brickColor);
    ui->lblBrickColorPreview->setText(bColor->name());
    ui->lblBrickColorPreview->setPalette(QPalette (*bColor));
    ui->lblBrickColorPreview->setAutoFillBackground(true);

    QColor * mColor = getQColor(mortarColor);
    ui->lblMortarColorPreview->setText(mColor->name());
    ui->lblMortarColorPreview->setPalette(QPalette (*mColor));
    ui->lblMortarColorPreview->setAutoFillBackground(true);

    ui->lblBrickSizeXVal->setText(QString::number(sizeX));
    ui->lblBrickSizeYVal->setText(QString::number(sizeY));
    ui->lblBrickFractionXVal->setText(QString::number(fractionX));
    ui->lblBrickFractionYVal->setText(QString::number(fractionY));

    ui->sliderBrickSizeX->setValue(sizeX * 99);
    ui->sliderBrickSizeY->setValue(sizeY * 99);
    ui->sliderBrickFractionX->setValue(fractionX * 99);
    ui->sliderBrickFractionY->setValue(fractionY * 99);
}

BrickShaderOptions::~BrickShaderOptions()
{
    delete ui;
}

void BrickShaderOptions::on_btnChooseMortarColor_clicked()
{
    QColor color = QColorDialog ::getColor(Qt::white, this);
    if (color.isValid())
    {
        mortarColor = getGLColor(color);
        ui->lblMortarColorPreview->setText(color.name());
        ui->lblMortarColorPreview->setPalette(QPalette (color));
        ui->lblMortarColorPreview->setAutoFillBackground(true);
        mainwindow->setMortarColor(mortarColor);
    }
}

void BrickShaderOptions::on_btnChooseBrickColor_clicked()
{
    QColor color = QColorDialog ::getColor(Qt::red, this);
    if (color.isValid())
    {
        brickColor = getGLColor(color);
        ui->lblBrickColorPreview->setText(color.name());
        ui->lblBrickColorPreview->setPalette(QPalette (color));
        ui->lblBrickColorPreview->setAutoFillBackground(true);
        mainwindow->setBrickColor(brickColor);
    }
}

QColor * BrickShaderOptions::getQColor(QVector3D *colorVector)
{
    return new QColor(colorVector->x() * 255, colorVector->y() * 255, colorVector->z() * 255);
}

/*
 * Converts a Qt hex color to a QVector3D for setting the color of a shader uniform value
 */
QVector3D *BrickShaderOptions::getGLColor(QColor color)
{
    QString colorName = color.name();
    qDebug() << colorName;
    QString strippedColor = colorName.mid(1, 7);
    qDebug() << strippedColor;


    QString rHex = strippedColor.mid(0, 2);
    QString gHex = strippedColor.mid(2, 2);
    QString bHex = strippedColor.mid(4, 2);

    qDebug() << rHex;
    qDebug() << gHex;
    qDebug() << bHex;

    bool ok;
    float R = rHex.toInt(&ok, 16) / 255.0f;
    float G = gHex.toInt(&ok, 16) / 255.0f;
    float B = bHex.toInt(&ok, 16) / 255.0f;

    qDebug() << R;
    qDebug() << G;
    qDebug() << B;

    QVector3D * vec = new QVector3D(R, G, B);

    return vec;
}

void BrickShaderOptions::on_buttonBox_accepted()
{
    mainwindow->setMortarColor(mortarColor);
    mainwindow->setBrickColor(brickColor);
    mainwindow->setBrickSize(brickSize);
    mainwindow->setBrickFraction(brickFraction);
}

void BrickShaderOptions::on_sliderBrickSizeX_valueChanged(int value)
{
    sizeX = value / 99.0f;
    ui->lblBrickSizeXVal->setText(QString::number(sizeX));
    brickSize->setX(sizeX);
    mainwindow->setBrickSize(brickSize);
}

void BrickShaderOptions::on_sliderBrickSizeY_valueChanged(int value)
{
    sizeY = value / 99.0f;
    ui->lblBrickSizeYVal->setText(QString::number(sizeY));
    brickSize->setY(sizeY);
    mainwindow->setBrickSize(brickSize);
}

void BrickShaderOptions::on_sliderBrickFractionX_valueChanged(int value)
{
    fractionX = value / 99.0f;
    ui->lblBrickFractionXVal->setText(QString::number(fractionX));
    brickFraction->setX(fractionX);
    mainwindow->setBrickFraction(brickFraction);
}

void BrickShaderOptions::on_sliderBrickFractionY_valueChanged(int value)
{
    fractionY = value / 99.0f;
    ui->lblBrickFractionYVal->setText(QString::number(fractionY));
    brickFraction->setY(fractionY);
    mainwindow->setBrickFraction(brickFraction);
}
