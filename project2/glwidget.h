#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "gl.h"
#include <QGLWidget>
#include <QElapsedTimer>
#include <QGLShader>
#include <QGLShaderProgram>
#include <QVector3D>
#include "obj.h"

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    void setObj(obj * o);

    void waveShader();
    void brickShader();
    void customShader(QString vertText, QString fragText, QString shaderName);

    void setBrickColor(QVector3D *color);
    void setMortarColor(QVector3D *color);
    void setBrickSize(QVector2D *size);
    void setBrickFraction(QVector2D *fraction);

    QVector3D *brickColor();
    QVector3D *mortarColor();
    QVector2D *brickSize();
    QVector2D *brickFraction();

    QString currentShader();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    QString m_currentShader;

    GLdouble rotation_x;
    GLdouble rotation_y;
    GLdouble position_z;
    GLdouble scale;

    GLdouble click_rotation_x;
    GLdouble click_rotation_y;
    GLdouble click_scale;
    GLdouble click_nx;
    GLdouble click_ny;

    obj * m_obj;

    QGLShaderProgram *ShaderProgram;
    QGLShader *VertexShader, *FragmentShader;

    QElapsedTimer *et;
    float sizeX;
    float sizeY;
    float fractionX;
    float fractionY;
    QVector3D *brick_color;
    QVector3D *mortar_color;
    QVector2D *brick_size;
    QVector2D *brick_frac;
};

#endif
