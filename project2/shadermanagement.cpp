#include "shadermanagement.h"
#include "ui_shadermanagement.h"
#include "shadereditor.h"
#include <QtGui>

ShaderManagement::ShaderManagement(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShaderManagement)
{
    ui->setupUi(this);
    list<<"Type"<<"Filename"<<"Compiled"<<"Active";
    ui->treeWidget->setHeaderLabels(list);
}

ShaderManagement::~ShaderManagement()
{
    delete ui;
}

void ShaderManagement::on_btnNew_clicked()
{
    ShaderEditor *sed = new ShaderEditor();
    sed->exec();
}

void ShaderManagement::on_btnClose_clicked()
{
    hide();
}

void ShaderManagement::on_btnOpen_clicked()
{
    QString selectedFilter;
    QStringList files = QFileDialog::getOpenFileNames(
                this, tr("Open a shader..."),
                "shader",
                tr("Fragment or Vertex Shader (*.frag *.vert)"),
                &selectedFilter);

    if (files.count())
    {
        QString file1 = "";
        QString file2 = "";

        file1 = files[0];
        if(files.count() == 2)
            file2 = files[1];

        GLchar * text1 = 0;
        GLchar * text2 = 0;
        GLuint fragShader = 0;
        GLuint vertShader = 0;
        QString file1Type;
        QString file2Type;
        QString file1Name;
        QString file2Name;
        QString file1BaseName;
        QString file2BaseName;

        if(!file1.isEmpty())
        {
            QFileInfo fi(file1);
            file1BaseName = fi.baseName();
            if(file1.contains(".frag"))
            {
                file1Type = "Fragment Shader";
                file1Name = fi.baseName() + ".frag";
            }

            if(file1.contains(".vert"))
            {
                file1Type = "Vertex Shader";
                file1Name = fi.baseName() + ".vert";
            }

            QTreeWidgetItem *item1 = new QTreeWidgetItem();
            item1->setText(0, file1Type);
            item1->setText(1, file1Name);
            item1->setToolTip(0, file1);
            item1->setToolTip(1, file1);
            item1->setToolTip(2, file1);
            item1->setToolTip(3, file1);
            ui->treeWidget->addTopLevelItem(item1);
        }

        if(!file2.isEmpty())
        {
            QFileInfo fi(file2);
            file2BaseName = fi.baseName();
            if(file2.contains(".frag"))
            {
                if(file1Type.compare("Fragment Shader") == 0)
                {
                    QMessageBox::warning(this, "Error", "Must pick a vertex shader to pair with the fragment shader.");
                    return;
                }
                file2Type = "Fragment Shader";
                file2Name = fi.baseName() + ".frag";
            }

            if(file2.contains(".vert"))
            {
                if(file1Type.compare("Vertex Shader") == 0)
                {
                    QMessageBox::warning(this, "Error", "Must pick a fragment shader to pair with the vertex shader.");
                    return;
                }
                file2Type = "Vertex Shader";
                file2Name = fi.baseName() + ".vert";
            }

            QTreeWidgetItem *item2 = new QTreeWidgetItem();
            item2->setText(0, file2Type);
            item2->setText(1, file2Name);
            item2->setToolTip(0, file2);
            item2->setToolTip(1, file2);
            item2->setToolTip(2, file2);
            item2->setToolTip(3, file2);
            ui->treeWidget->addTopLevelItem(item2);
        }

        ui->treeWidget->resizeColumnToContents(0);
        ui->treeWidget->resizeColumnToContents(1);
        ui->treeWidget->resizeColumnToContents(2);
        ui->treeWidget->resizeColumnToContents(3);
    }
}

void ShaderManagement::vertexShaderCompileError(char *message)
{
    QMessageBox::critical(this, "GLSL Compiler Error", "There was an error compiling the vertex shader: \n" + QString(message));
}

void ShaderManagement::fragmentShaderCompileError(char *message)
{
    QMessageBox::critical(this, "GLSL Compiler Error", "There was an error compiling the fragment shader: \n" + QString(message));
}

void ShaderManagement::programLinkError(char *message)
{
    QMessageBox::critical(this, "GLSL Linker Error", "There was an error linking the program: \n" + QString(message));
}

void ShaderManagement::on_btnDelete_clicked()
{

}

void ShaderManagement::on_btnApply_clicked()
{

}

void ShaderManagement::on_btnEdit_clicked()
{
    ShaderEditor *sed = new ShaderEditor();
    sed->exec();
}
