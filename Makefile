
CC= gcc
CFLAGS= -ansi -pedantic -Wall -g -std=c99

#-------------------------------------------------------------------------------

ifeq ($(shell uname), Linux)
OGLLIB= -lglut -lGLEW -lGL -ljpeg -lpng -ltiff -lz -lm
else ifeq ($(shell uname), Darwin)
OGLLIB= -lGLEW -framework GLUT -framework OpenGL -ljpeg -lpng -ltiff -lz -lm
else
OGLLIB= -lfreeglut -lglew32 -lopengl32 -lwinmm -lgdi32 -ljpeg -lpng -ltiff -lz -lm
endif

#-------------------------------------------------------------------------------

%.o : %.c
	$(CC) $(CFLAGS) -c $<

project2 : project2.o
	$(CC) $(CFLAGS) -o $@ $^ $(OGLLIB)

clean :
	rm -f project2 project2.o
	
rebuild : clean project2

