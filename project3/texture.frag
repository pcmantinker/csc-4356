uniform sampler2D diffuse;
uniform sampler2D normal;
uniform sampler2D specular;
uniform sampler2D spot;

varying vec4 var_L;
varying vec4 var_V;

void main()
{
    float att = clamp(1.0 - dot(var_V.xyz, var_V.xyz), 0.0, 1.0);
    vec3 V = normalize(var_V.xyz);
    vec3 L = normalize(var_L.xyz);

    vec3 N = texture2D(normal, gl_TexCoord[0].xy).rgb; // Normal Texture
    vec4 D = texture2D(diffuse, gl_TexCoord[0].xy); // Diffuse Texture
    vec4 S = texture2D(specular, gl_TexCoord[0].xy); // Specular Texture
    vec4 G = texture2DProj(spot, gl_TexCoord[3]); // Spotlight Texture

    N = normalize(2.0 * N - 1.0);

    vec3 R = reflect(L, N);
    float n = gl_FrontMaterial.shininess;
    vec4 ka = gl_FrontMaterial.ambient * gl_LightModel.ambient;
    vec4 kd = gl_FrontMaterial.diffuse * clamp(dot(L, N), 0.0, 1.0);
    vec4 ks = gl_FrontMaterial.specular * pow(max(dot(R, V), 0.0), n);

    vec4 C = (D * kd + S * ks) * G + (D * ka);

    gl_FragColor = C;
}
