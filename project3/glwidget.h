#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "gl.h"
#include <QGLWidget>
#include <QElapsedTimer>
#include <QGLShader>
#include <QGLShaderProgram>
#include <QVector3D>
#include "obj.h"

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    void setObj(obj * o);

    void setDiffuseTexture(QImage image);
    void setNormalTexture(QImage image);
    void setSpecularTexture(QImage image);
    void customShader(QString vertText, QString fragText);
    void load13BoxTextures();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    GLdouble rotation_x;
    GLdouble rotation_y;
    GLdouble position_z;
    GLdouble scale;

    GLdouble click_rotation_x;
    GLdouble click_rotation_y;
    GLdouble click_scale;
    GLdouble click_nx;
    GLdouble click_ny;

    GLdouble nx;
    GLdouble ny;
    GLdouble spotlight_x;
    GLdouble spotlight_y;

    obj * m_obj;

    QGLShaderProgram *ShaderProgram;
    QGLShader *VertexShader, *FragmentShader;

    QElapsedTimer *et;

    GLuint diffuse;
    GLuint specular;
    GLuint normal;
    GLuint spot;

    void loadShaderProgram();
};

#endif
