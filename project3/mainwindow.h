#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector3D>
#include "glwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    GLWidget *glWidget();
    
private slots:
    void on_actionExit_triggered();

    void on_actionAbout_Qt_triggered();

    void on_actionAbout_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_Framebuffer_triggered();

    void on_actionToolbar_triggered();

    void on_actionAbout_Open_GL_triggered();

    void on_actionDiffuse_triggered();

    void on_actionSpecular_triggered();

    void on_actionNormal_triggered();

private:
    Ui::MainWindow *ui;
    QString openFilesPath;
    QString saveFileName;
    GLWidget *m_glWidget;

};

#endif // MAINWINDOW_H
