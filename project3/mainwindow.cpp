#include "gl.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "about.h"
#include "aboutopengl.h"
#include "obj.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_glWidget = new GLWidget();
    this->setCentralWidget(m_glWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionExit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_actionAbout_triggered()
{
    About * about = new About();
    about->exec();
}

void MainWindow::on_actionOpen_triggered()
{
    QString selectedFilter;
    QStringList files = QFileDialog::getOpenFileNames(
                this, tr("Open a Wavefront model..."),
                openFilesPath,
                tr("Wavefront (*.obj)"),
                &selectedFilter);
    if (files.count()) {
        openFilesPath = files[0];
        qDebug() << "Loading " + openFilesPath + "...";
        if(openFilesPath.contains(".obj"))
        {
            QFileInfo fi(openFilesPath);
            obj * o = obj_create(openFilesPath.toStdString().c_str());
            setWindowTitle("Project 3 - " + fi.baseName() + ".obj");
            qDebug() << "Object loaded at address " << o;
            m_glWidget->setObj(o);
        }
    }
}

void MainWindow::on_actionSave_Framebuffer_triggered()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save an image..."),
                                                    QString("image"),
                                                    tr("PNG Image (*.png);;JPEG Image (*.jpg, *.jpeg);;GIF Image (*.gif);;All Files (*)"),
                                                    &selectedFilter);
    m_glWidget->grabFrameBuffer(false).save(fileName, "PNG");
}

void MainWindow::on_actionToolbar_triggered()
{

}

GLWidget * MainWindow::glWidget()
{
    return m_glWidget;
}

void MainWindow::on_actionAbout_Open_GL_triggered()
{
    AboutOpenGL *aboutGL = new AboutOpenGL();
    aboutGL->exec();
}

void MainWindow::on_actionDiffuse_triggered()
{

}

void MainWindow::on_actionSpecular_triggered()
{

}

void MainWindow::on_actionNormal_triggered()
{

}
