#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include <QtGui>

class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = 0);
    ~About();

    bool eventFilter(QObject *obj, QEvent *event);

public slots:
    void okClicked();

};

#endif // ABOUT_H
